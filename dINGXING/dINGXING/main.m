//
//  main.m
//  dINGXING
//
//  Created by 刘畅 on 14-9-4.
//  Copyright (c) 2014年 刘畅. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
